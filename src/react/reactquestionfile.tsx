import * as React from "react";
import { SurveyQuestionElementBase } from "./reactquestionelement";
import { QuestionFileModel } from "../question_file";
import { ReactQuestionFactory } from "./reactquestionfactory";

import Modal from 'react-responsive-modal';

export class SurveyQuestionFile extends SurveyQuestionElementBase {
  constructor(props: any) {
    super(props);
    this.state = { fileLoaded: 0, open:false, src:'' };
    this.handleOnChange = this.handleOnChange.bind(this);
    this.openModal = this.openModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
  }
  protected get question(): QuestionFileModel {
    return this.questionBase as QuestionFileModel;
  }
  handleOnChange(event) {
    var src = event.target || event.srcElement;
    if (!window["FileReader"]) return;
    if (!src || !src.files || src.files.length < 1) return;

    this.setState({ fileLoaded: this.state.fileLoaded + 1 });
  }

  openModal(src){
    this.setState({open:true, src: src.nativeEvent.target.currentSrc})
  }

  onCloseModal(){
    this.setState({open:false})
  }

  render(): JSX.Element {
    if (!this.question) return null;
    var img = [] ;
    var imgItem;
    var modal;
    var fileInput = null;
    if (!this.isDisplayMode) {
      if (this.question.value !== undefined){
        var src='';
        var temImage = JSON.parse(this.question.value);
        temImage.map((item) => {
          src = 'https://v4.dpsmturkey.com/' + item
          imgItem = <div style={{ margin: 15, display: 'inline-block'}}><img  src={src} onClick={this.openModal} height={200} width={200} /></div>
          img.push(imgItem)
        })
      } else {
        img =[ <div style={{ textAlign:'center' }}><p>Soru için çekilen fotoğraf bulunamadı.</p></div>]
      }
      {
        modal = <Modal open={this.state.open} onClose={this.onCloseModal} little>
        <img  src={this.state.src} onClick={this.openModal} height={600} width={600} />
      </Modal>
      }
    } else {
      fileInput = (
        <input
          id={this.question.inputId}
          type="file"
          onChange={this.handleOnChange}
          aria-label={this.question.locTitle.renderedHtml}
        />
      );
    }

    return (
      <div>
        {fileInput}
        {img}
        {modal}
      </div>
    );
  }

}

ReactQuestionFactory.Instance.registerQuestion("file", props => {
  return React.createElement(SurveyQuestionFile, props);
});

